package com.teamlab.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeaturesList {
	
	/**
	 * ngrams(n for n-grams, String) : This methods generates the n-grams for the String.
	 * concat(words List,startValue,endValue) : This methods concatenate the words from the String into respective n-gram.
	 */
	public List<String> ngrams(int n, String str) {
        List<String> ngrams = new ArrayList<String>();
        String[] words = str.split(" ");
        for (int i = 0; i < words.length - n + 1; i++)
            ngrams.add(concat(words, i, i+n));
        return ngrams;
    }

    public  String concat(String[] words, int start, int end) {
        StringBuilder sb = new StringBuilder();
        for (int i = start; i < end; i++)
            sb.append((i > start ? " " : "") + words[i]);
        return sb.toString();
    }
    
    
	/**
	 * generateNegList : This method generates the list of negative words.
	 */
	public List<String> generateNegList() {
		List<String> negationList = new ArrayList<String>();
		BufferedReader br = null;
		String line;
		try {
			br = new BufferedReader(new FileReader("/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/NegationDictionary"));
			while((line = br.readLine())!=null) {
			    negationList.add(line.trim());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				br.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		return negationList;
	}
	
	/**
	 * getPolarityFeature() : This method maps the emotion with its polarity.
	 */
	
	public Map<String,List<String>> getPolarityFeature() {
		
		Map<String,List<String>> emotionPolarityMap = new HashMap<String,List<String>>();
		List<String> emotionList = new ArrayList<String>();
		BufferedReader br = null;
		String line ;
		try {
			 br = new BufferedReader(new FileReader("/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/NRC-emotion-lexicon.txt"));
			 String wordKey = "";
				List<String> emotions = null;
				while ((line = br.readLine()) != null) {
					String elem[] = line.split("	");
					if (Integer.parseInt(elem[2]) == 0) {
						continue;
					}
					if (wordKey.length() == 0) {
						wordKey = elem[0];
						emotions = new ArrayList<String>();
					}
					if (!(wordKey.equalsIgnoreCase(elem[0]))) {
						emotionPolarityMap.put(wordKey, emotions);
						wordKey = elem[0];
						emotions = new ArrayList<String>();
					}

					emotions.add(elem[1]);
				}

				emotionPolarityMap.put(wordKey, emotions);
			 
			 
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return emotionPolarityMap;
	}
	
	/**
	 * getEmojisListWithPolarity() : This method maps the emojis with its respective polarity : postive,negative,neutral etc.
	 */
	public Map<String,String> getEmojisListWithPolarity() {
		Map<String,String> emojiMap = new HashMap<String,String>();
 		BufferedReader br = null;
		String line;
		 try {
			 br = new BufferedReader(new FileReader("/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/emojisWithPolarity.txt"));
			 while((line=br.readLine())!=null) {
				 String[] word = line.split("  	");
				 String[] emojis = word[0].split("\\s"); 
				  for(String emoji : emojis) {
					   if(!emoji.equals("")) {
						   emojiMap.put(emoji, word[1]);
					   }  
				}
			 }
		 }
		 catch(Exception e) {
			 e.printStackTrace();
			 
		}
		 finally {
			 try {
				 br.close();
			 }
			 catch(Exception e) {
				 e.printStackTrace();
			 }
		 }
		 return emojiMap;
	}
	
	/**
	 * getDiscourseFeature() : This method creates the discourse dictionary
	 */
	public Map<String,String> getDiscourseFeature() {
		Map<String,String> discourseMap = new HashMap<String,String>();
 		BufferedReader br = null; 
		String line;
		try {
			br = new BufferedReader(new FileReader("/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/DiscourseDictionary"));
			while((line=br.readLine())!=null) {
				String[] word = line.split(":");
				String[] wordList = word[1].trim().split(",");
				for(String item : wordList) {
					discourseMap.put(item, word[0].trim());
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				br.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return discourseMap;
	}
	
	/**
	 * posTaggerMap() : This method creates a map between words and its respective part of speech tags.
	 */
	public Map<String,String> posTaggerMap() {
		 Map<String,String> posTagMap = new HashMap<String,String>();
		 BufferedReader br =  null;
		 String line;
		 
		 try {
			br = new BufferedReader(new FileReader("/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/tagdict.csv"));
			 while((line = br.readLine())!=null) {
				  String[] word = line.split("\\s");
				  posTagMap.put(word[0], word[1]);
				 
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 finally {
			 try {
				 br.close();
			 }
			 catch(Exception e) {
				 e.printStackTrace();
			 }
		 }
		 return posTagMap;
	 }

}
