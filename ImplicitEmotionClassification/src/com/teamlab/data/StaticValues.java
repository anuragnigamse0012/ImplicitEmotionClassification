package com.teamlab.data;

/**
 * Description : This method is used to store all the static values used in the application.
 * @author Anurag
 *
 */
public class StaticValues {
	
	public static final String trainingData = "/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/TestTrain.csv";
	
	public static final String testData = "/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/test.csv";
	
	public static final String readGoldLabels = "/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/test.labels";
	
	public static final String writeProcessTweet = "/home/anurag/Documents/Project/ImplicitEmotionClassification/ImplicitEmotionClassification/Data/preProcessedTweet.csv";
	
	public static final String writeProcessTestTweet = "/home/anurag/Documents/preProcessedTestTweet.csv";
	
	public static String CONNECTIVES = "but,however,nevertheless,otherwise,yet,still,nonetheless,therefore,furthermore,consequently,thus,eventually,hence";
	public static String CONJ_PREV = "till , until , despite , inspite , though , although";
	public static String CONDITIONALS = "if,might,could,can,would,may";
	
	public static final String tabSpace = "\t";
	
	public static final String newLine = "\n";
	
	public static final int biGram = 2;
	
	public static final int triGram = 3;
	
	private static StaticValues value;
	
	public static synchronized StaticValues getInstance( ) { 
		 value = new StaticValues();
		 return value;
	}
}
