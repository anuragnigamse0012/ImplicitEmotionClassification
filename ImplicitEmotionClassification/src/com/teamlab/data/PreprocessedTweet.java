package com.teamlab.data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tartarus.snowball.ext.EnglishStemmer;

import com.teamlab.utils.Tweet;

/**
 * Description : This class is used to preprocess the data set. All the preprocessing steps are described here.
 * @author Anurag
 *
 */
public class PreprocessedTweet {
		
	private static PreprocessedTweet preProcess;
	
	List<String> preprocessedTweet = new ArrayList<String>();
	
	StaticValues staticValue = StaticValues.getInstance();
	
	/*FeatureExtractor fExt = FeatureExtractor.getInstance();
	
	public synchronized static PreprocessedTweet getInstance() {
		preProcess = new PreprocessedTweet();
		return preProcess;
	}*/
	
	
	public void preProcessedData(List<Tweet> tweets) {
		List<String> stopWordList = new ArrayList<String>();
		  
		StringBuffer textProcess = null;
		for(Tweet tweetData : tweets) {
		      
		      
			   
			  /* Preprocessing : Replacing the words starting with @,# with empty and http://url.removed to url
			   * 
			   */
			  String preProcessed = tweetData.getText().replaceAll("@[A-Za-z]+","")  // Remove words with @
					  				.replaceAll("#(?!TRIGGERWORD)[A-Za-z]+","")  // Remove words with # except #TRIGGERWORD
					  				.replaceAll("^@\\w+", "").replaceAll("http://url.removed", "URL")  // Replace http://url.removed with URL
					  				.replaceAll("^\\s+|\\s+$|\\s+(?=\\s)","") // Remove the whiteSpaces
					  				.replaceAll("[|^0-9|#|!|.|]","") // Remove the numbers
					  				.toLowerCase();
			  	
			  
			  //Remove stopWord from the tweet
			  stopWordList = getStopWordList();
			  textProcess = new StringBuffer();
			  String[] words = preProcessed.trim().split("\\s+");
			  for(int i=0; i<words.length; i++) {
				  if(!stopWordList.contains(words[i]) && !textProcess.toString().contains(words[i])){
					    		textProcess.append(words[i].trim());
					    		textProcess.append(" ");
					}
			  }
			  preProcessed = textProcess.toString();
			  
			  tweetData.setPreProcessedTweet(preProcessed);
			  //preprocessedTweet.add(preProcessed);
		
		
		}
	}
	
	/**
	 * Collects the preprocess data set on file.
	 * @param listTweet
	 * @param flag
	 * @throws IOException
	 */
	public void getProcessedTweetOnFile(List<Tweet> listTweet, Boolean flag) throws IOException {
		
		BufferedWriter fw  = null;
		String fileName = StaticValues.writeProcessTweet;
		if(flag) {
			fileName = StaticValues.writeProcessTestTweet;
		}
		
		try {
		  fw = new BufferedWriter(new FileWriter(fileName));
			for(Tweet tweet : listTweet) {
				fw.append(tweet.getGold());
				fw.append(StaticValues.tabSpace);
				fw.append(tweet.getPreProcessedTweet());
				fw.append(StaticValues.tabSpace);
				fw.append(StaticValues.newLine);
			}
		}
		
		catch (IOException e) {
			
				e.printStackTrace();
		}
		fw.close();
	}
	
	
	
	
	
	public List<String> getStopWordList() {
		String line;
		List<String> stopWordList = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("/home/anurag/Documents/EmotionAnalysis/Data/stopwords-list.txt"));
			while((line = br.readLine()) != null) {
				stopWordList.add(line.trim());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return stopWordList;
	}
	
	/**
	 * Used for stemming. We used Snowball stemmer for stemming purpose.
	 * @param token
	 * @return
	 */
	public String stemToken(String token) {
		  
	  	EnglishStemmer stemmer = new EnglishStemmer();
		stemmer.setCurrent(token);
		
			if(stemmer.stem()) {
				token = stemmer.getCurrent();
			}
   			 
	return token; 
   }
	
	public void removeDuplicateTweets(List<Tweet> listTweet) {
		
		  
			for(Tweet tweet : listTweet) {
				int similarityValue = 0;
				int i = 0;
				Tweet firstTweet = listTweet.get(i);
					while(!((firstTweet.getPreProcessedTweet()).isEmpty()) && (i < listTweet.size())) {
							
						    Tweet secondTweet =  listTweet.get(i+1);
						   // similarityValue = getCosineSimilarity(firstTweet,secondTweet,preprocessedTweet);
						    
						    if(similarityValue > 0.5) {
						    	
						    }
							
						}
					i++;
		  }
		
		}
	
	/*public int getCosineSimilarity(Tweet firstTweet,Tweet secondTweet, List<String> preProcessedTweet) {
		int value = 0;
			List<String> firstTweetToken = new ArrayList<String>();
			List<String> secondTweetToken = new ArrayList<String>();
			List<Integer> wordInFirstTweet = new ArrayList<Integer>();
			List<Integer> wordInSecondTweet = new ArrayList<Integer>();
			
			Set<String> uniqueWordList = new HashSet<String>();
			firstTweetToken = fExt.tokenize(firstTweet);
			secondTweetToken = fExt.tokenize(secondTweet);
			uniqueWordList = getUniqueWordList(firstTweetToken,secondTweetToken);
			for(String word : uniqueWordList) {
				wordInFirstTweet.add(firstTweet.getPreProcessedTweet().contains(word) ? 1 : 0);
				wordInSecondTweet.add(secondTweet.getPreProcessedTweet().contains(word) ? 1 : 0);
			}
			
			for(String tweet : preProcessedTweet) {
				   
			}
		
		return value;
	}*/
	
	public Set<String> getUniqueWordList(List<String> firstTweet, List<String> secondTweet) {
		Set<String> wordList = new HashSet<String>();
		  for(String word : firstTweet) {
			  wordList.add(word);
		}
		  for(String word : secondTweet) {
			  wordList.add(word);
		  }
		
		return wordList;
	}
		
}

