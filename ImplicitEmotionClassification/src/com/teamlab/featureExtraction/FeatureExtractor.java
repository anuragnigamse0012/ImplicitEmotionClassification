package com.teamlab.featureExtraction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.teamlab.data.FeaturesList;
import com.teamlab.data.PreprocessedTweet;
import com.teamlab.data.StaticValues;
import com.teamlab.utils.Tweet;

public class FeatureExtractor {
	
	/**
	 * @author anurag
	 *
	 * This is the implementation for the features used for experiment 1 mentioned on the paper. This class
	 * creates the feature vector. 
	 */

	private static FeatureExtractor featureExtractor;
	private String line;

	FeaturesList featureList = new FeaturesList();
	PreprocessedTweet preProcess = new PreprocessedTweet(); 
	
	Map<String,List<String>> polarityMap = featureList.getPolarityFeature();
	
	Map<String,String> emojiMap = featureList.getEmojisListWithPolarity();
	
	Map<String,String> discourseMap = featureList.getDiscourseFeature();
	
	Map<String,String> posTag = featureList.posTaggerMap();
	
	List<String> negList = featureList.generateNegList();
	
	

	public synchronized static FeatureExtractor getInstance() {
		if (featureExtractor == null) {
			featureExtractor = new FeatureExtractor();
		}
		return featureExtractor;
	}
	/**
	 * This method reads the stop words from a file.
	 * @return stopList
	 */

	public List<String> getStopList() {
		List<String> stopList = new ArrayList<String>();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(
					"/home/anurag/git/ImplicitEmotionClassification/EmotionClassification/ImplicitEmotionClassification_julia/ImplicitEmotionClassification_julia/Files/stopwords-list.txt"));
			while ((line = br.readLine()) != null) {
				stopList.add(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stopList;
	}
	
	/**
	 * Breaks the text into tokens.
	 * 
	 * @return Arraylist of tokens. 
	 */

	public List<String> tokenize(String text) {
		if(text != null) {
			Pattern pattern = Pattern.compile("\\s+|[,.:!?\"\'-\\(\\)]+\\s+");
			String[] tokens = pattern.split(text.toLowerCase());

			return Arrays.asList(tokens);
		}
		return new ArrayList<String>();
	}


	
	/*public void extractFeatures(Tweet tweet) {
		List<String> features = new ArrayList<String>();
		for (String token : tweet.getTokens()) {
			//token = preProcess.stemToken(token);
			String feature = "w=" + token;
			features.add(feature);
		}
		
		tweet.setFeatures(features);
	}
*/
	
	/** This method is used for incrementally applying features to the model.
	 * @param tweet
	 * @throws IOException
	 */
	public void extractFeatures(Tweet tweet) throws IOException {
	 
		Map<String,Double> features = new HashMap<String, Double>();
		//Map<String, Double> features = new HashMap<String, Double>();
		extractFeatureAsUnigram(tweet,features);
		extractFeatureAsBigram(tweet,features);
		extractFeatureAsTrigram(tweet,features);
		//extractFeatureWithNegationDict(tweet, features);
		//getNegationDiscourseFeature(tweet,features);
		//extractFeatureWithDiscourseDict(tweet,features);
		//extractFeatureWithPolarity(tweet,features);
		extractFeatureWithEmojiPolarity(tweet,features);
		extractFeatureWithPOSTag(tweet,features);	
	
		
	}
	/**
	 * Generate the unigrams from the tweet.
	 * @param tweet
	 * @param features
	 */
	
	public void extractFeatureAsUnigram(Tweet tweet, Map<String,Double> features) {
		
		
		for (String token : tweet.getTokens()) {
			//token = preProcess.stemToken(token);
			String unigram = "w=" + token;
			features.put(unigram,1.0);
		}
		
		tweet.setFeatures(features);
	}
	
	/**
	 * Generates the bigram from the text.
	 * @param tweet
	 * @param features
	 */
	public void extractFeatureAsBigram(Tweet tweet, Map<String, Double> features) {
		String text = tweet.getText();
		List<String> biGram = featureList.ngrams(StaticValues.biGram, text);
		for(String biGramWord : biGram) {
			features.put(biGramWord, 1.0);
		}
		tweet.setFeatures(features);
	}
	
	/**
	 * Generates the trigram from the text.
	 * @param tweet
	 * @param features
	 */
	public void extractFeatureAsTrigram(Tweet tweet, Map<String, Double> features) {
		String text = tweet.getText();
		List<String> triGram = featureList.ngrams(StaticValues.triGram, text);
		for(String triGramWord : triGram) {
			features.put(triGramWord, 1.0);
		}
		tweet.setFeatures(features);
	}
	
	/**
	 * Generates the Negation features from the negation dictionary, comparing words in the text.
	 * @param tweet
	 * @param features
	 */
	
	public void extractFeatureWithNegationDict(Tweet tweet, Map<String, Double> features) {
		
		String[] wordList = tweet.getText().split(" ");
		for(String word : wordList) {
			if(negList.contains(word)) {
				  if(features.containsKey(word)) {
					  features.put(word,features.get(word) - 1.0);
				  }
				  else {
					  features.put(word,-1.0);
				  }
				 
			}
		}
		tweet.setFeatures(features);
		
	}
	
	/**
	 * Generates the negation discourse feature.
	 * @param tweet
	 * @param features
	 */
	
	public void getNegationDiscourseFeature(Tweet tweet, Map<String, Double> features) {
		String[] wordList = tweet.getText().split(" ");
		List<String> discourseWord = Arrays.asList(StaticValues.CONNECTIVES.split(","));
		
		int negation_window = 0;
		for (String word : wordList) {
			if(negList.contains(word.trim())){
				negation_window = 3;
				continue;
			}
			
			if(negation_window > 0){
				if(discourseWord.contains(word.trim())){
					break;
				}
				features.put("Negation-"+word, 1.0);
				negation_window--;
			}
			
		}
	}
	
	/**
	 * Generates the discourse features. 
	 * @param tweet
	 * @param features
	 * @throws IOException
	 */
	public void extractFeatureWithDiscourseDict(Tweet tweet, Map<String, Double> features) throws IOException {
		String[] wordList = tweet.getText().split(" ");
		List<String> conditionals = Arrays.asList(StaticValues.CONDITIONALS.split(","));
		List<String> connectives = Arrays.asList(StaticValues.CONNECTIVES.split(","));
		boolean connectiveWord = false;
		for (String word : wordList) {
			
			double weight = 1;
			if(conditionals.contains(word.trim())){
				features.put("condi_"+word, 1.0);
				
			}
			else if(connectives.contains(word.trim())){
				connectiveWord = true;
			}
			
			if(connectiveWord){
				
				weight = weight+1;
			}
			features.put("conn_"+word, weight);
			
		}
	}
	
	/**
	 * Generates the polarity feature from the NRC lexicon.
	 * @param tweet
	 * @param features
	 */
	public void extractFeatureWithPolarity(Tweet tweet,Map<String, Double> features) {
		
		String[] wordList = tweet.getText().split(" ");
		  for(String word : wordList) {
			   
			   if(polarityMap.containsKey(word)) {
				   List<String> emotionList = polarityMap.get(word); 
				     for(String emotion : emotionList) {
				    	   String featureName  = "EmotionType="+emotion.trim();
				    	   if(features.containsKey(featureName)){
				    		   features.put(featureName, features.get(featureName) + 1.0);
				    	   }
				    	   else {
				    	   features.put(featureName, 1.0);
				    	   }
				     }
				   
			   }
		  }
		  
		  tweet.setFeatures(features);
	}
	
	/**
	 * Generates the Emoji features.
	 * @param tweet
	 * @param features
	 */
	public void extractFeatureWithEmojiPolarity(Tweet tweet,Map<String, Double> features) {
		String[] wordList = tweet.getText().split(" ");
		  for(String word : wordList) {
			   
			   if(emojiMap.containsKey(word)) {
				   String emojiPolarity = emojiMap.get(word); 
				   		String feature = word;
				           if(emojiPolarity.equalsIgnoreCase("Positive")) {
				        	   features.put(feature,1.0);
				           }
				           else if(emojiPolarity.equalsIgnoreCase("Negative")) {
				        	   features.put(feature,-1.0);
				           }
				           else if(emojiPolarity.equalsIgnoreCase("Neutral")) {
				        	    features.put(feature, 0.0);
				           }
				           
				    	   
				    	   
			   }
		  }
		  
		  tweet.setFeatures(features);
	}
	
	/**
	 * Generates the features from the POS tag dictionary.
	 * @param tweet
	 * @param features
	 */
	public void extractFeatureWithPOSTag(Tweet tweet, Map<String, Double> features) {
		String[] wordList = tweet.getText().split(" ");
		Map<String, Double> posMap = new HashMap<String, Double>();
		for(String word : wordList) {
				if(posTag.containsKey(word)) {
					String posTagger = posTag.get(word);
					 String featureName = "POS="+ posTagger;
					 if(features.containsKey(word)) {
						 posMap.put(featureName, features.get(word) + 1.0);
					 }
					 else {
						 posMap.put(featureName, 1.0 );
					 }
					
				}
		}
		String label = Collections.max(posMap.entrySet(), Comparator.comparing(Map.Entry::getValue)).getKey();
		double value = Collections.max(posMap.entrySet(), Comparator.comparing(Map.Entry::getValue)).getValue();
		features.put(label, value);
		tweet.setFeatures(features);
	}

}
