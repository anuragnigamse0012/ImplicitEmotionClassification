package com.teamlab.featureExtraction;

import java.util.HashMap;
import java.util.Map;

import com.teamlab.evaluation.process.Evaluation;
import com.teamlab.utils.Corpus;
import com.teamlab.utils.Tweet;

public class TF_IDF {
	
	private static TF_IDF tf_idf;
	
	private Corpus corpus;
	private Map<Tweet, Map<String, Double>> tf;
	private Map<String, Double> idf;
	
	 public static TF_IDF getInstance() {
		   if(tf_idf == null) {
			   synchronized (TF_IDF.class){
				   tf_idf = new TF_IDF();
			   }
		   }
	   
	   return tf_idf;
	   }
	
	 private Map<String, Double> tf(Tweet tweet) {
		Map<String, Double> tfMap = new HashMap<String, Double>();
		
		for(String token : tweet.getTokens()) {
			if(!tfMap.containsKey(token)) {
				tfMap.put(token, 1.0);
			}
			else {
				tfMap.put(token, tfMap.get(token) + 1);
			}
		}
		
		//log frequency weighting
		for(String token : tfMap.keySet()) {
			double weightedTF = (1 + Math.log10(tfMap.get(token)));
			tfMap.put(token, weightedTF);
		}	
		return tfMap;	 
	 }

	 
	 public void getTF() {
		 
	 }

	public Corpus getCorpus() {
		return corpus;
	}

	public void setCorpus(Corpus corpus) {
		this.corpus = corpus;
	}
	 
	 
}
