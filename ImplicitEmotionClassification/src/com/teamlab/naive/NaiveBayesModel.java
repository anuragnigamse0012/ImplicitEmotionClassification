package com.teamlab.naive;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.teamlab.featureExtraction.FeatureExtractor;
import com.teamlab.utils.Corpus;
import com.teamlab.utils.Tweet;

/**
 * @author Anurag
 *
 * This is the implementation for naive Bayes model
 */

public class NaiveBayesModel {
	
	private static NaiveBayesModel naiveBayes;
	
	FeatureExtractor feature = FeatureExtractor.getInstance();
	Corpus corpus = Corpus.getInstance();
	
	private static Map<String,Double> priorProbability;
	
	private static Set<String> unique;
	
	private static int uniqueCount = 0;
	
	public static NaiveBayesModel getInstance() {
		naiveBayes = new NaiveBayesModel();
		
		return naiveBayes;
	}
	
	 /** calculatePriorProbabilities : This methods takes the parameter as the tweets and label set.
	  *
	  * It calculates the prior probability for each label.
	  */
	 
	public Map<String,Double> calculatePriorProbabilities(List<Tweet> tweets,String[] labels) {
		
		
		Map<String,Double> classCount = new HashMap<String,Double>();
		priorProbability = new HashMap<String,Double>();
	
		for(Tweet tweet : tweets) {
			
			double count = classCount.containsKey(tweet.getGold()) ? classCount.get(tweet.getGold()).doubleValue()+1 : 1.0;
	    	classCount.put(tweet.getGold(), count);
	    	priorProbability.put(tweet.getGold(), (classCount.get(tweet.getGold()).doubleValue())/tweets.size());
		}
		
		return priorProbability;
	}
	
	
	 /** getUniqueCount : This methods calculates the count of the words in the vocabulary.
	 */
	
	/*public void getUniqueCount(List<Tweet> tweets) {
		 unique = new HashSet<String>();
		for(Tweet tweetData : tweets) {
			unique.addAll(tweetData.getFeatures());
			
		}
		uniqueCount = unique.size();
		
	}*/
	
	  /**labelWordMap : This methods takes the parameter as the tweets, label set and .
	   	*It calculates the prior probability for each label.
	   	*/
	 
	
	
	public Map<String,Map<String,Double>> labelWordMap(List<Tweet> tweets){
		Map<String,Map<String,Double>> labelWordMap = new HashMap<String,Map<String,Double>>();
		Map<String,Map<String,Double>> labelWordProbabilityMap = new HashMap<String,Map<String,Double>>();
		double denominator = 0;
			for(Tweet tweetData : tweets) {
						 String label = tweetData.getGold();
	
						 if (!labelWordMap.containsKey(label)) {
							 labelWordMap.put(label, new HashMap<String, Double>());
						 }
						 
						 for(String token : tweetData.getTokens()) {
							  	if(!(token.equalsIgnoreCase(" "))) {
							  		double updatedCount = (labelWordMap.get(label).containsKey(token)) ?
							  		labelWordMap.get(label).get(token) + 1 : 1.0;
							        labelWordMap.get(label).put(token, updatedCount);
							}
						 }
					}
			
			for(Map.Entry<String, Map<String,Double>> m : labelWordMap.entrySet()) {
				    Map<String,Double> wordCountMap =  new HashMap<String,Double>();
				    
				    
				    for(Map.Entry<String, Double> wordMap : m.getValue().entrySet()) {
				    		 
				    		 denominator = getDenominatorValue(m.getValue());
				    		 wordCountMap.put(wordMap.getKey(),(wordMap.getValue()/denominator));
				    		 labelWordProbabilityMap.put(m.getKey(), wordCountMap);
				    }
				    
			}
			
			for(Map.Entry<String, Map<String,Double>> m : labelWordMap.entrySet()) {
				System.out.println("Value \t:"+m.getKey() + "\t" + m.getValue().toString());
			}
	
		return labelWordMap;
	}
	
	 /**getDenominatorValue : This method provides the total frequency of the words needed in calculating
	 *						 the conditional probability. 
	 */
	
	public double getDenominatorValue(Map<String,Double> totalWordValue) {
		double denominator = 0;
		
		for(Map.Entry<String,Double> total : totalWordValue.entrySet()) {
			  denominator = denominator + total.getValue();
		}
		return denominator;
	}
	
	
	 /** calculateTotalProbability : This method takes the parameter as labelWordMap and list of tweets. 
	 * This methods calculate the Posterior probability.
	 */
	 
	
	public void calculateTotalProbability(Map<String,Map<String,Double>> labelWordMap, List<Tweet> tweets) {
		Map<String,Double> labelInstanceMap ; 
		
		double totalProbability = 0;
		//int iteration = 10;
			for(Tweet tweet : tweets) {
				labelInstanceMap = new HashMap<String,Double>();
				
					for(Map.Entry<String,Map<String,Double>> labelWord : labelWordMap.entrySet()) {
						double wordValue = 1.0;
						double denominator = getDenominatorValue(labelWord.getValue());
						 	for(String token : tweet.getTokens()) {
			        	               
			        	   			if(labelWord.getValue().containsKey(token)) {
			        	   				 wordValue = wordValue * ((labelWord.getValue().get(token) + 1)/(denominator + uniqueCount));
			        	   			}
			        	   			else {
			        	   				 wordValue =  wordValue * (1/(denominator + uniqueCount));
			        	   			}
			            		       
			            		 }
			           
			        totalProbability = priorProbability.containsKey(labelWord.getKey()) ? (priorProbability.get(labelWord.getKey()) * (wordValue)): wordValue;
			        labelInstanceMap.put(labelWord.getKey(), totalProbability);
			            	  
			 }
			 String label = Collections.max(labelInstanceMap.entrySet(), Comparator.comparing(Map.Entry::getValue)).getKey();
	    	 //System.out.println("Predicted Value : "+label);
			 tweet.setPredicted(label);
	    	
			
			
		    /*for (Map.Entry<String, Map<String,Double>> pair : labelWordMap.entrySet()) {
						Map<String, Double> p = pair.getValue();
						double wordValue = 0.0;
						for(String token : tweet.getTokens()) {
						   
							denominator = getDenominatorValue(labelWordMap,token);
							wordValue = p.containsKey(token) ? wordValue + Math.log10(p.get(token).doubleValue()/denominator) : wordValue;  
						}
						
						totalProbability = priorProbability.containsKey(pair.getKey()) ? Math.log10(priorProbability.get(pair.getKey()).doubleValue())+wordValue : 0.0;
						labelInstanceMap.put(pair.getKey(), totalProbability);
			  		}
		    
		    		String label = Collections.max(labelInstanceMap.entrySet(), Comparator.comparingDouble(Map.Entry::getValue)).getKey();
		    		tweet.setPredicted(label);
			  			*/
			//}
		}
	}
}

