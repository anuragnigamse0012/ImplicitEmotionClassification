package com.teamlab.ml;

import java.util.HashMap;
import java.util.Map;

import com.teamlab.utils.Corpus;
import com.teamlab.utils.Tweet;

/**
 * Description : This class implements Averaged Perceptron algorithm for training and testing.
 * @author Anurag
 */
public class MulticlassAveragedPerceptron {

	private static MulticlassAveragedPerceptron multiAvgPerceptron;
	private Map<String, AveragedPerceptron> perceptrons;
	int count;
	public synchronized static MulticlassAveragedPerceptron getInstance() {	
		multiAvgPerceptron = new MulticlassAveragedPerceptron();
		multiAvgPerceptron.perceptrons = new HashMap<String, AveragedPerceptron>();
		for(String label : Corpus.LABELS) {
			multiAvgPerceptron.perceptrons.put(label, AveragedPerceptron.getInstance());
		}
		return multiAvgPerceptron;
	}

	/***
	 * This method does the prediction for each tweet.
	 * @param tweet
	 */
	public void predict(Tweet tweet) {

		double maxScore = 0;
		String predicted = "NONE";
		count ++;
		for(String label : perceptrons.keySet()) {
			AveragedPerceptron perceptron = perceptrons.get(label);
			double score = perceptron.getAveragedScore(tweet.getFeatures());

			if(score > maxScore)
			{
				maxScore = score;
				predicted = label;
			}
		}
		tweet.setPredicted(predicted);
	}

	/**
	 * This method is used to train the corpus.
	 * @param corpus
	 * @param numIterations
	 */
	public void train(Corpus corpus, int numIterations) {
		count = 0;
		for(int i = 0; i < numIterations; i++) {
			for(Tweet tweet : corpus.getTweets()) {
				predict(tweet);
				update(tweet,count);
				
			}
		}
		finalWeight();
		
	}

	/**
	 * This method is used to update the perceptron.
	 * @param tweet
	 */
	private void update(Tweet tweet, int count) {
		
		//if predicted was wrong, update perceptrons
		if(!tweet.getPredicted().equals(tweet.getGold())) {
			if(!tweet.getPredicted().equals("NONE"))
				perceptrons.get(tweet.getPredicted()).decrease(tweet.getFeatures(),count);
			
			perceptrons.get(tweet.getGold()).increase(tweet.getFeatures(), count);
		}
		//else, do nothing
	}
	
	public void finalWeight() {
		for(String label : perceptrons.keySet()) {
			AveragedPerceptron perceptron = perceptrons.get(label);
			perceptron.getFinalWeight(count);
			
		}
	}
	
	public void predictTest(Tweet tweet) {

		double maxScore = 0;
		String predicted = "NONE";
		
		for(String label : perceptrons.keySet()) {
			AveragedPerceptron perceptron = perceptrons.get(label);
			double score = perceptron.getAveragedScoreTest(tweet.getFeatures());

			if(score > maxScore)
			{
				maxScore = score;
				predicted = label;
			}
		}
		tweet.setPredicted(predicted);
	}
	
	/**
	 * This method is used to predict the test data set.
	 * @param tweet
	 */
	public void predictTest(Corpus corpus) {
		
		for(Tweet tweet : corpus.getTweets()) {
			predictTest(tweet);
		}
	}

}
