package com.teamlab.ml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AveragedPerceptron {

	private static AveragedPerceptron perceptron;
	private Map<String, Double> weights;
	private Map<String,Double> averageWeights;
	private Map<String,Double> finalWeights;

	public synchronized static AveragedPerceptron getInstance() {	
		perceptron = new AveragedPerceptron();
		perceptron.weights = new HashMap<String, Double>();
		perceptron.averageWeights = new HashMap<String, Double>();
		perceptron.finalWeights = new HashMap<String, Double>();
		return perceptron;
	}

	private Double getWeight(String feature) {
		if(weights.containsKey(feature))
			return weights.get(feature);
		else
			return 0.0;
	}
	
	private Double getAverageWeight(String feature) {
		if(averageWeights.containsKey(feature))
			return averageWeights.get(feature);
		else
			return 0.0;
	}

	public double getAveragedScore(Map<String,Double> features) {
		double score = 0.0;
		for(String feature : features.keySet()) {
			score += getWeight(feature);
			getAverageWeight(feature);
		}
		return score;
	}
	
	public double getAveragedScoreTest(Map<String, Double> features) {
		double score = 0.0;
		for(String feature : features.keySet()) {
			score += getWeight(feature);
		}
		return score;
	}
	
	

	public void increase(Map<String, Double> features, int count) {
		for(String feature: features.keySet()) {
			if(weights.containsKey(feature)) {
				weights.put(feature, weights.get(feature) + features.get(feature));
				averageWeights.put(feature,weights.get(feature) * count);
			}
			else {
				weights.put(feature, features.get(feature));
				averageWeights.put(feature,features.get(feature));
			}
		}
	}

	public void decrease(Map<String, Double> features, int count) {
		for(String feature: features.keySet()) {
			if(weights.containsKey(feature)) {
				weights.put(feature, weights.get(feature) - features.get(feature));
				averageWeights.put(feature,weights.get(feature) * count);
			}
			else {
				weights.put(feature, features.get(feature) * count);
				averageWeights.put(feature,features.get(feature) * count);
			}
		}
	}
	
	
	public void getFinalWeight(int count) {
		
		for(String key : weights.keySet()) {
			weights.put(key, weights.get(key).doubleValue() - (averageWeights.get(key).doubleValue())/count);
		}
	}
	
	
}
