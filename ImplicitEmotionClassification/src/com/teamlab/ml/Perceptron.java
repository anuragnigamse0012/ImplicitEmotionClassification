package com.teamlab.ml;


import java.util.HashMap;
import java.util.Map;

public class Perceptron {

	private static Perceptron perceptron;
	private Map<String, Double> weights;
	public synchronized static Perceptron getInstance() {	
		perceptron = new Perceptron();
		perceptron.weights = new HashMap<String, Double>();
		
		return perceptron;
	}

	private Double getWeight(String feature) {
		if(weights.containsKey(feature))
			return weights.get(feature);
		else
			return 0.0;
	}
	
	public double getScore(Map<String, Double> features) {
		double score = 0.0;
		for(String feature : features.keySet()) {
			score += getWeight(feature);
			}
		return score;
	}

	public void increase(Map<String, Double> features) {
		for(String feature: features.keySet()) {
			if(weights.containsKey(feature))
				weights.put(feature, weights.get(feature) + features.get(feature));
			else
				weights.put(feature, features.get(feature));
		}
	}

	public void decrease(Map<String, Double> features) {
		for(String feature: features.keySet()) {
			if(weights.containsKey(feature))
				weights.put(feature, weights.get(feature) - features.get(feature));
			else
				weights.put(feature, features.get(feature));
		}
	}
	
}
