package com.teamlab.ml;

import java.util.HashMap;
import java.util.Map;

import com.teamlab.utils.Corpus;
import com.teamlab.utils.Tweet;

/**
 * Description : This class implements Perceptron algorithm for training and testing.
 * @author Anurag
 */

public class MulticlassPerceptron {

	private static MulticlassPerceptron multiPerceptron;
	private Map<String, Perceptron> perceptrons;
	
	public synchronized static MulticlassPerceptron getInstance() {	
		multiPerceptron = new MulticlassPerceptron();
		multiPerceptron.perceptrons = new HashMap<String, Perceptron>();
		for(String label : Corpus.LABELS) {
			multiPerceptron.perceptrons.put(label, Perceptron.getInstance());
		}
		return multiPerceptron;
	}

	/***
	 * This method does the prediction for each tweet.
	 * @param tweet
	 */
	public void predict(Tweet tweet) {

		double maxScore = 0;
		String predicted = "NONE";

		for(String label : perceptrons.keySet()) {
			Perceptron perceptron = perceptrons.get(label);
			double score = perceptron.getScore(tweet.getFeatures());

			if(score > maxScore)
			{
				maxScore = score;
				predicted = label;
			}
		}
		tweet.setPredicted(predicted);
	}

	/**
	 * This method is used to train the corpus.
	 * @param corpus
	 * @param numIterations
	 */
	public void train(Corpus corpus, int numIterations) {
		
		for(int i = 0; i < numIterations; i++) {
			for(Tweet tweet : corpus.getTweets()) {
					predict(tweet);
					update(tweet);
			}
		}
		
	}

	/**
	 * This method is used to update the perceptron.
	 * @param tweet
	 */
	private void update(Tweet tweet) {
		
		//if predicted was wrong, update perceptrons
		if(!tweet.getPredicted().equals(tweet.getGold())) {
			if(!tweet.getPredicted().equals("NONE"))
				perceptrons.get(tweet.getPredicted()).decrease(tweet.getFeatures());
			
			perceptrons.get(tweet.getGold()).increase(tweet.getFeatures());
		}
		//else, do nothing
	}
	
	/**
	 * This method is for predicting for the test data set.
	 * @param corpus
	 */
	public void predict(Corpus corpus) {
		
		for(Tweet tweet : corpus.getTweets()) {
			predict(tweet);
		}
	}

}
