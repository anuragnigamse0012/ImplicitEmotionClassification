package com.teamlab.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description : This class is used to extract values from the tweet.
 * @author Anurag
 *
 */

public class Tweet implements Serializable{
	
	 
	//private String lhs; //before target word
	//private String rhs; //after target word
	private String text; //no distinction between lhs and rhs for now
	private String gold; //correct label
	private String predicted; //predicted label
	private String preProcessedTweet;
	private int id;
	private List<String> tokens;
	
	private Map<String, Double> features;
	
	public Tweet(int id, String text, String gold, String predicted) {
		//this.lhs = lhs;
		//this.rhs = rhs;
		this.id = id;
		this.text = text;
		this.gold = gold;
		this.predicted = predicted;
		this.tokens = new ArrayList<String>();
		this.features = new HashMap<String,Double>();
	}

/*	public String getLhs() {
		return lhs;
	}

	public void setLhs(String lhs) {
		this.lhs = lhs;
	}

	public String getRhs() {
		return rhs;
	}

	public void setRhs(String rhs) {
		this.rhs = rhs;
	}
	
	
*/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getGold() {
		return gold;
	}

	public void setGold(String gold) {
		this.gold = gold;
	}

	public String getPredicted() {
		return predicted;
	}

	public void setPredicted(String predicted) {
		this.predicted = predicted;
	}

	@Override
	public String toString() {
		return "Tweet [id=" + id + ", text=" + text + ", gold=" + gold + ", predicted=" + predicted + "]";
	}

	public List<String> getTokens() {
		return tokens;
	}

	public void setTokens(List<String> tokens) {
		this.tokens = tokens;
	}

	public Map<String, Double> getFeatures() {
		return features;
	}

	public void setFeatures(Map<String, Double> features) {
		this.features = features;
	}
	
	public String getPreProcessedTweet() {
		return preProcessedTweet;
	}

	public void setPreProcessedTweet(String preProcessedTweet) {
		this.preProcessedTweet = preProcessedTweet;
	}

}
