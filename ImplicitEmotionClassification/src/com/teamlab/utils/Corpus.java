package com.teamlab.utils;

import java.util.ArrayList;
import java.util.List;

import com.teamlab.evaluation.process.EvaluationResults;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Description : This class is used to read the data from the corpus.
 * @author anurag
 *
 */
public class Corpus {
	private List<Tweet> tweets;
	private static Corpus corpus;
	//private final File src;
	
	public static final String[] LABELS = {"sad", "joy", "disgust", "surprise", "anger", "fear"};
	
	public Corpus() {
		tweets = new ArrayList<Tweet>();
		//src = new File(filename);
	}
	
	public synchronized static Corpus getInstance() {

		corpus = new Corpus();

		return corpus;
	}
	
	public void readFile(String filename) {
		try {
			tweets.clear();
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String line;
			int id = 1;
			while((line = in.readLine()) != null) 
			{
				String[] fields = line.split("\t");
				Tweet tweet = new Tweet(id, fields[1].trim(), "NONE", fields[0].trim());
				//String[] fields = line.split(",");
				//Tweet tweet = new Tweet(id, "NONE", fields[0], fields[1]);
				tweets.add(tweet);
				id++;
			}
			in.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//read a file containing gold values for tweets
	public void readGold(String filename) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(new File(filename)));
			String line;
			int i = 0;
			while((line = in.readLine()) != null) 
			{
				Tweet t = tweets.get(i);
				t.setGold(line.trim());
				tweets.set(i, t);	
				i++;
			}
			in.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void readTraining(String filename) {
		try {
			tweets.clear();
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String line;
			int id = 1;
			while((line = in.readLine()) != null) 
			{
				String[] fields = line.split("\t");
				Tweet tweet = new Tweet(id, fields[1].trim(), fields[0].trim(), "NONE");
				tweets.add(tweet);
				id++;
			}
			in.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public List<Tweet> getTweets() {
		return tweets;
	}

	public void setTweets(List<Tweet> tweets) {
		this.tweets = tweets;
	}

}
