package com.teamlab.evaluation.process;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import com.teamlab.utils.Corpus;
import com.teamlab.utils.Tweet;

/**
 * Description: This is the class where the model is evaluated based on the TP, FP, FN for each label.
 * @author anurag
 *
 */
public class Evaluation {
    
	   private static Evaluation evaluation;
	    
	   public static Evaluation getInstance() {
		   if(evaluation == null) {
			   synchronized (Evaluation.class){
				   evaluation = new Evaluation();
			   }
		   }
	   
	   return evaluation;
	   }
	      
	   Map<String,Double> accurate = new HashMap<String,Double>();
	   
	   /**
	    * Calculates the TP, FP, FN.
	    * @param corpus
	    * @param label
	    * @return
	    */
	   public EvalTriple labelMetrics(Corpus corpus, String label) {
		   //true positives
		   Stream<Tweet> truePos = corpus.getTweets().stream()
				   .filter(t -> (t.getPredicted().equals(label)) && (t.getGold().equals(label))); 
		   
		   //false positives
		   Stream<Tweet> falsePos = corpus.getTweets().stream()
				   .filter(t -> (t.getPredicted().equals(label)) && (!t.getGold().equals(label)));
			
		  //false negatives
		   Stream<Tweet> falseNeg = corpus.getTweets().stream()
				   .filter(t -> (!t.getPredicted().equals(label)) && (t.getGold().equals(label)));
		   
		 /*  //true negatives
		   Stream<Tweet> trueNeg = corpus.getTweets().stream()
				   .filter(t -> (!t.getPredicted().equals(label)) && (!t.getGold().equals(label)));
		   */
		   
		
		   return new EvalTriple((int) truePos.count(), (int) falsePos.count(), (int) falseNeg.count());
	   }
	   
	 /**
	  * Calculates the precision value for each label.
	  * @param corpus
	  * @param label
	  * @return
	  */
	  public double precision(Corpus corpus, String label) {
		   EvalTriple triple = labelMetrics(corpus, label);
		   int denominator = (triple.getTruePositive() + triple.getFalsePositive());
		   if(denominator == 0)
			   return 0;  
	
		   return ((double) triple.getTruePositive() /denominator);
	   }
	   
	   /**
		  * Calculates the recall value for each label.
		  * @param corpus
		  * @param label
		  * @return
		  */
	   public double recall(Corpus corpus, String label) {
		   EvalTriple triple = labelMetrics(corpus, label);
		   int denominator = (triple.getTruePositive() + triple.getFalseNegative());
		   if(denominator == 0)
			   return 0;   
		   return ((double) triple.getTruePositive() /denominator);
	   }
	   
	   /**
		  * Calculates the f-score value for each label.
		  * @param corpus
		  * @param label
		  * @return
		  */
	   public double f_score(Corpus corpus, String label) {
		   double precision = precision(corpus, label);
		   double recall = recall(corpus, label);
		   
		return (double) ((2 * (precision * recall))/(precision + recall));	   
	   }
	   
	   /**
	    * Calculates the accuracy. 
	    * @param corpus
	    * @return
	    */
	   
	   /*public double accuracy(Corpus corpus, String label) {
		   EvalTriple triple = labelMetrics(corpus, label);
		   int denominator = (triple.getTruePositive()+triple.getFalsePositive()+triple.getFalseNegative()+triple.getTrueNegative());
		    if(denominator == 0)
		    	return 0;
		    return (((double) (triple.getTruePositive()+triple.getTrueNegative()) / denominator) * 100);
	   }*/
	   
	 /**
	  * Calculates the micro precision value.
	  * @param corpus
	  * @return
	  */
	   public double precision_micro(Corpus corpus) {
		   int numerator = 0;
		   int denominator = 0;
		   for(String label : Corpus.LABELS) {
			   EvalTriple triple = labelMetrics(corpus, label);
			   numerator += triple.getTruePositive();
			   denominator += (double) (triple.getTruePositive() + triple.getFalsePositive());
		   }
		   return ((double) numerator/denominator);
	   }
	   
	   /**
		  * Calculates the micro recall value.
		  * @param corpus
		  * @return
		  */
	   public double recall_micro(Corpus corpus) {
		   int numerator = 0;
		   int denominator = 0;
		   for(String label : Corpus.LABELS) {
			   EvalTriple triple = labelMetrics(corpus, label);
			   numerator += triple.getTruePositive();
			   denominator += (double) (triple.getTruePositive() + triple.getFalseNegative());
		   }
		   return ((double) numerator/denominator);
	   }
	
	  /**
	   * Calculates the micro f-score.
	   * @param corpus
	   * @return
	   */
	   public double f_score_micro(Corpus corpus) {
		   return (double)(((2 * (precision_micro(corpus) * recall_micro(corpus))) / (precision_micro(corpus) + recall_micro(corpus))));
	   }
	   
	 /**
	  * Calculates the macro precision value.
	  * @param corpus
	  * @return
	  */
	   public double precision_macro(Corpus corpus) {
		   double numerator = 0;
		   for(String label : Corpus.LABELS) {
			   numerator += precision(corpus, label);
		   }
		   return ((double) numerator/Corpus.LABELS.length);
	   }
	   
	 /**
	  * Calculates the macro recall value.
	  * @param corpus
	  * @return
	  */
	   public double recall_macro(Corpus corpus) {
		   double numerator = 0;
		   for(String label : Corpus.LABELS) {
			   numerator += recall(corpus, label);
		   }
		   return ((double) numerator/Corpus.LABELS.length);
	   }
	   
	   /**
	    * Calculates the macro f-score value.
	    * @param corpus
	    * @return
	    */
	   public double f_score_macro(Corpus corpus) {
		   return (double)(((2 * (precision_macro(corpus) * recall_macro(corpus))) / (precision_macro(corpus) + recall_macro(corpus))));
	   }
}
