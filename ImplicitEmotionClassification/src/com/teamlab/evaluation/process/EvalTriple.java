package com.teamlab.evaluation.process;

/**
 * This is the setter and getter class for the TP, FP, FN.
 * @author Anurag
 *
 */
public class EvalTriple {
	
	private int truePositive;
	private int falsePositive;
	private int falseNegative;
	//private int trueNegative;
	
	public EvalTriple() {
		truePositive = 0;
		falsePositive = 0;
		falseNegative = 0;
		//trueNegative = 0;
	}
	
	public EvalTriple(int truePos, int falsePos, int falseNeg) {
		
		this.truePositive = truePos;
		this.falsePositive = falsePos;
		this.falseNegative = falseNeg;
		//this.trueNegative = trueNeg;
	}

	public int getTruePositive() {
		return truePositive;
	}

	public void setTruePositive(int truePositive) {
		this.truePositive = truePositive;
	}

	public int getFalsePositive() {
		return falsePositive;
	}

	public void setFalsePositive(int falsePositive) {
		this.falsePositive = falsePositive;
	}

	public int getFalseNegative() {
		return falseNegative;
	}

	public void setFalseNegative(int falseNegative) {
		this.falseNegative = falseNegative;
	}
	
	/*public int getTrueNegative() {
		return trueNegative;
	}

	public void setTrueNegative(int trueNegative) {
		this.trueNegative = trueNegative;
	}*/
	
	@Override
	public String toString() {
		return "EvalTriple [truePositive=" + truePositive + ", falsePositive=" + falsePositive + ", falseNegative="
				+ falseNegative + "]";
	}
	
}
