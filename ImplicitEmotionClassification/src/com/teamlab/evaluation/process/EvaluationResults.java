package com.teamlab.evaluation.process;

import java.io.IOException;
import java.util.List;

import com.teamlab.data.PreprocessedTweet;
import com.teamlab.data.StaticValues;
import com.teamlab.featureExtraction.FeatureExtractor;
import com.teamlab.ml.MulticlassAveragedPerceptron;
import com.teamlab.ml.MulticlassPerceptron;
import com.teamlab.utils.Corpus;
import com.teamlab.utils.Tweet;


/**
 * Description : This class is the main class which runs the model and the displays the results for each model.
 * @author Anurag
 *
 */
public class EvaluationResults {

	private static EvaluationResults evalResults;

	public synchronized static EvaluationResults getInstance() {

		evalResults = new EvaluationResults();

		return evalResults;
	}


	public static void main(String args[]) {
		EvaluationResults evaluationResults = EvaluationResults.getInstance();
		FeatureExtractor fExt = FeatureExtractor.getInstance();
		Corpus training = new Corpus();
		Corpus test = new Corpus();
		training.readTraining(StaticValues.trainingData);
		test.readFile(StaticValues.testData);
		test.readGold(StaticValues.readGoldLabels);
		PreprocessedTweet preProcessTweet = new PreprocessedTweet();

		/**
		 * This is the implementation for the naive Bayes model
		 */
  /* ------------------------------------- Naive Bayes Approach ----------------------------------------------------- */ 
		
		/*NaiveBayesModel naiveBayes = NaiveBayesModel.getInstance();
		List<Tweet> tweets = training.getTweets();
		tweets.forEach(tweet -> tweet.setTokens(fExt.tokenize(tweet.getText())));
		//tweets.forEach(tweet -> tweet.setFeatures(fExt.extractFeatures(tweet)));
		tweets.forEach(tweet -> fExt.extractFeatures(tweet));
		naiveBayes.getUniqueCount(tweets);
		naiveBayes.calculatePriorProbabilities(tweets,Corpus.LABELS);
		Map<String,Map<String,Double>> labelWordMap = naiveBayes.labelWordMap(tweets);
		//naiveBayes.calculateTotalProbability(labelWordMap, classProbability, Corpus.LABELS, tweets);
		
		NaiveBayesModel naiveBayesTest = NaiveBayesModel.getInstance();
		FeatureExtractor fExtTest = FeatureExtractor.getInstance();
		
		
		List<Tweet> tweetsTest = test.getTweets();
		tweetsTest.forEach(tweet -> tweet.setTokens(fExtTest.tokenize(tweet.getText())));
		//tweetsTest.forEach(tweet -> tweet.setFeatures(fExtTest.extractFeatures(tweet)));
		tweetsTest.forEach(tweet -> fExt.extractFeatures(tweet));
		//naiveBayesTest.calculatePriorProbabilities(tweetsTest,test.LABELS);
		//Map<String,Map<String,Double>> labelWordMapTest = naiveBayesTest.labelWordMap(tweetsTest);
		naiveBayesTest.calculateTotalProbability(labelWordMap,tweetsTest); 

		//training.readTraining("/home/anurag/Documents/Teamlab/train.csv");
		//test.readFile("/home/anurag/Documents/Teamlab/trial.csv");
		

		//List<Tweet> tweets = training.getTweets();
		preProcessTweet.preProcessedData(tweets);
		try {
			preProcessTweet.getProcessedTweetOnFile(tweets);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//tweets.forEach(tweet -> tweet.setPreProcessedTweet(preProcessTweet.preProcessedData(tweet)));
		//training.readTraining("/home/anurag/Documents/preProcessedTweet.csv");
*/		
	/*-----------------------------------Perceptron Modelling -----------------------------------------------*/
		
		/**
		 * This is the implementation for the perceptron model.
		 */
		
		List<Tweet> tweets = training.getTweets();
		preProcessTweet.preProcessedData(tweets);
		try {
			preProcessTweet.getProcessedTweetOnFile(tweets,false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tweets.forEach(tweet -> tweet.setTokens(fExt.tokenize(tweet.getText())));
		tweets.forEach(tweet -> {
			try {
				fExt.extractFeatures(tweet);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
		MulticlassPerceptron multiPerceptron = MulticlassPerceptron.getInstance();
		multiPerceptron.train(training, 5);
		
		Evaluation eval = Evaluation.getInstance();
		for(String label : Corpus.LABELS) {
			System.out.println("label : " + label);
			System.out.println("LABEL METRICS: " + eval.labelMetrics(training, label));
			System.out.println("PRECISION: " + eval.precision(training, label));
			System.out.println("RECALL: " + eval.recall(training, label));
			System.out.println("F-SCORE: " + eval.f_score(training, label));
			//System.out.println("ACCURACY: "+eval.accuracy(test, label));
			System.out.println();
		}

		System.out.println("MICRO PRECISION: " + eval.precision_micro(training));
		System.out.println("MICRO RECALL: " + eval.recall_micro(training));
		System.out.println("MICRO F: " + eval.f_score_micro(training));
		System.out.println();
		System.out.println("MACRO PRECISION: " + eval.precision_macro(training));
		System.out.println("MACRO RECALL: " + eval.recall_macro(training));
		System.out.println("MACRO F: " + eval.f_score_macro(training)); 
		
		System.out.println("****** TESTING *******");

		List<Tweet> testTweets = test.getTweets();
		testTweets.forEach(tweet -> tweet.setTokens(fExt.tokenize(tweet.getText())));		
		testTweets.forEach(tweet -> {
			try {
				fExt.extractFeatures(tweet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		//testprocessTweets.forEach(tweet -> fExt.extractFeatureWithPolarity(tweet));
		multiPerceptron.predict(test);
		
		
		
	/**
	 * This is implementation for averaged perceptron.
	 */
		
	/* --------------------------------- Averaged Perceptron ---------------------------------------------- */
		/*List<Tweet> tweets = training.getTweets();
		tweets.forEach(tweet -> tweet.setTokens(fExt.tokenize(tweet.getText())));
		tweets.forEach(tweet -> {
			try {
				fExt.extractFeatures(tweet);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
		MulticlassAveragedPerceptron multiAvgPerceptron = MulticlassAveragedPerceptron.getInstance();
		multiAvgPerceptron.train(training, 15);
		System.out.println("****** TESTING *******");

		List<Tweet> testTweets = test.getTweets();
		//testTweets.forEach(tweet -> tweet.setPreProcessedTweet(preProcessTweet.preProcessedData(tweet)));
		try {
			boolean flag = true;
			preProcessTweet.getProcessedTweetOnFile(testTweets,flag);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		testTweets.forEach(tweet -> tweet.setTokens(fExt.tokenize(tweet.getText())));		
		testTweets.forEach(tweet -> {
			try {
				fExt.extractFeatures(tweet);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		multiAvgPerceptron.predictTest(test);*/
		
		
	/* ---------------------------------------------------------------------------------------------------- */ 	

		
		/*----------------------------------- Evaluation Starts here  -----------------------------------------------*/

		Evaluation evaluation = Evaluation.getInstance();
		for(String label : Corpus.LABELS) {
			System.out.println("label : " + label);
			System.out.println("LABEL METRICS: " + evaluation.labelMetrics(test, label));
			System.out.println("PRECISION: " + evaluation.precision(test, label));
			System.out.println("RECALL: " + evaluation.recall(test, label));
			System.out.println("F-SCORE: " + evaluation.f_score(test, label));
			//System.out.println("ACCURACY: "+evaluation.accuracy(test, label));
			System.out.println();
		}

		System.out.println("MICRO PRECISION: " + evaluation.precision_micro(test));
		System.out.println("MICRO RECALL: " + evaluation.recall_micro(test));
		System.out.println("MICRO F: " + evaluation.f_score_micro(test));
		System.out.println();
		System.out.println("MACRO PRECISION: " + evaluation.precision_macro(test));
		System.out.println("MACRO RECALL: " + evaluation.recall_macro(test));
		System.out.println("MACRO F: " + evaluation.f_score_macro(test)); 
	}
}
